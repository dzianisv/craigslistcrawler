#!/usr/bin/env python3
import os
import binascii
import time
import sys
import telegram
import telegram.ext
import logging
import threading
import json
import pickle
from craigslist import CraigslistForSale
from pymongo import MongoClient

from app import config
from app import db
from app import bot

class State(object):
    def __init__(self):
        state = next(db.state.find({}), None)
        if state is not None:
            self._id = state['_id']
            self.update_id = state['update_id']
        else:
            self._id = None
            self.update_id = None

    def save(self):
        if self._id is not None:
            db.state.update_one({'_id': self._id}, {'$set': {'update_id': self.update_id}})
        else:
            db.state.insert_one({'update_id': self.update_id})

def start(u, ctx):
    logging.info("subscribing %d", u.message.chat_id)
    db.subscribers.insert_one({
        'telegram_chat_id': u.message.chat_id,
        'filter': {
            "search_distance": 50,
            "zip_code": 94025,
            "min_price": 2000,
            "max_price": 4600,
            "min_year": 2008,
            "max_miles": 150000,
            "auto_title_status": ['clean']
        },
        'latest': 0
    })
    u.message.reply_text('Subscribed')

def stop(u, ctx):
    logging.info("unsubscribing %d", u.message.chat_id)
    db.subscribers.delete_many({
        'telegram_chat_id': u.message.chat_id
    })
    u.message.reply_text('Unsubscribed')

def telegram_events_loop():
    # webhook_token = binascii.b2a_base64(os.urandom(128))
    updater = telegram.ext.Updater(token=config.telegram_token, use_context=True)
    updater.dispatcher.add_handler(telegram.ext.CommandHandler('start', start))
    updater.dispatcher.add_handler(telegram.ext.CommandHandler('stop', start))
    # updater.start_webhook(listen='0.0.0.0',
                    #   port=8000,
                    #   url_path=webhook_token,
                    #   webhook_url=f'{config.telegram_webhook}/{webhook_token}')
    updater.start_polling()
    updater.idle()
    
if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    telegram_events_loop()